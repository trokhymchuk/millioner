"use strict";

function shuffle(array) {
        var currentIndex = array.length,
                temporaryValue,
                randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
        }

        return array;
}

let worldlist = [{
                issue: "Жіноче ім'я із страшних снів Біла Клінтона",
                answer: ["Міоніка", "Яга", "Марія", "Хіларі"],
                ranswer: "Міоніка",
        },
        {
                issue: "Ким став Аленушкіни братик Іванко, випивши водиці з сумнівного джерела?",
                answer: ["Оленем", "Поросям", "Сливкою-буркою", "Козеням"],
                ranswer: "Козеням",
        },
        {
                issue: "Що видають школярам на початку навчального року?",
                answer: ["Стипендію", "Правду в очі", "Атестат", "Підручники"],
                ranswer: "Підручники",
        },
        {
                issue: "Як називається популярний рецепт приготування макаронів з м'ясом?",
                answer: ["По-братськи", "По-флотськи", "По-божому", "По-сільському"],
                ranswer: "По-флотськи",
        },
        {
                issue: "Як звали героя казки Шарля Перро?",
                answer: [
                        "Хлопчик з щіпку",
                        "Хлопчик-невидимка",
                        "Хлопчик з нігтик",
                        "Хлопчик з пальчик",
                ],
                ranswer: "Хлопчик з пальчик",
        },
        {
                issue: "Де прожив останні роки Ернест Хемингуэй?",
                answer: ["Іспанія", "Гаїті", "Майамі", "Куба"],
                ranswer: "Куба",
        },
        {
                issue: "Що каже нам Windows XP при запуску?",
                answer: ["Система не працює", "Вітання", "Побажання", "Екран смерті"],
                ranswer: "Вітання",
        },
        {
                issue: "Як називають знавця багатьох мов?",
                answer: ["Поліграф", "Поліглот", "Полімер", "Поліесп"],
                ranswer: "Поліглот",
        },
        {
                issue: "Хто з цих філософів в 1864 році написав музику на вірші А. С. Пушкіна «Заклинання» і «Зимовий вечір»?",
                answer: ["Гегель", "Ніцше", "Юнг", "Шопенгауер"],
                ranswer: "Ніцше",
        },
        {
                issue: "Скільки разів у добу подзаводят куранти Спаської башти Кремля?",
                answer: ["Один", "Два", "Три", "Чотири"],
                ranswer: "Два",
        },
        {
                issue: "Хто 1-го отримав Нобелівську премію з літератури?",
                answer: ["Есеїст", "Поет", "Драматург", "Романіст"],
                ranswer: "Поет",
        },
        {
                issue: "З якої букви починаються слова, опубліковані в 16-му томі останнього видання Великої радянської енциклопедії?",
                answer: ["П", "Про", "Н", "М"],
                ranswer: "М",
        },
        {
                issue: "Хто з перерахованих був пажем у часи Катерини II?",
                answer: ["Н.М. Карамзін", "А. Н. Радищев", "Державін Р.", "Фонвізін Д. І."],
                ranswer: "А. Н. Радищев",
        },
        {
                issue: "Який хімічний елемент названий на честь злого підземного гнома?",
                answer: ["Гафній", "Кобальт", "Берилій", "Телур"],
                ranswer: "Кобальт",
        },
        {
                issue: "В якій з цих столиць союзних республік раніше з'явилося метро?",
                answer: ["Мінськ", "Баку", "Єреван", "Тбілісі"],
                ranswer: "Тбілісі",
        },
        {
                issue: "Скільки морів омивають Балканський півострів?",
                answer: ["6", "3", "4", "5"],
                ranswer: "6",
        },
        {
                issue: "Що таке лобогрейка?",
                answer: ["Жнейка", "Шапка", "Хвороба", "Пічка"],
                ranswer: "Жнейка",
        },
        {
                issue: "Який роман Фенімор Купер написав на спір з дружиною?",
                answer: ["'Звіробій'", "'Останній з могікан'", "'Піонери'", "'Обережність'"],
                ranswer: "'Обережність'",
        },      
        {
                issue: "Який вид кавалерії призначався для бойових дій як в кінному, так і в пішому строю??",
                answer: ["Гусари", "Драгуни", "Улани", "Кирасиры"],
                ranswer: "Драгуни",
        },
        {
                issue: "Яке ім'я не приймав жоден папа римський?",
                answer: ["Віктор", "Георгій", "Євген", "Валентин"],
                ranswer: "Георгій",
        },
        {
                issue: "В якому німецькому місті народилася майбутня імператриця Росії Катерина ІІ?",
                answer: ["Дармштадт", "Штеттін", "Цербст", "Вісбаден"],
                ranswer: "Штеттін",
        },
        {
                issue: "Що забороняв указ, який в 1726 році підписала Катерина I?",
                answer: ["Переливати з пустого в порожнє", "Пускати пил в очі", "Бити байдики", "Точити ляси"],
                ranswer: "Пускати пил в очі",
        },        {
                issue: "Хто носить темні окуляри?",
                answer: ["Лисі", "Кульгаві", "П'яні'", "Сліпі"],
                ranswer: "Поет",
        },       ];

//let toask = worldlist[Math.floor(Math.random() * worldlist.length)];
// var pname = prompt("Ім'я першого гравця");
// document.querySelector("#youtplay").innerHTML = pname;

let qlist = [];

while(qlist.length != 15){
        let question =  worldlist[Math.floor(Math.random() * worldlist.length)];
        if (qlist.indexOf(question) === -1)
                qlist.push(question);
}

console.log(qlist);

let score = 0,
        qnum = 1;
let scoremas = [
        100,
        200,
        300,
        500,
        1000,
        2000,
        4000,
        8000,
        16000,
        32000,
        64000,
        125000,
        250000,
        500000,
        1000000,
];
let boolean = true;
let isright = false;
/////////////////////////////////////////////////////////////////////////////////
setanswer();

function setanswer() {
        if (qnum > 1)
                document.getElementById("p" + (qnum - 1)).classList.toggle("curschet");
        document.getElementById("p" + qnum).classList.toggle("curschet");

        //let toask = worldlist[Math.floor(Math.random() * worldlist.length)];
        let toask = qlist[qnum - 1];
        document.querySelector("#qask").innerHTML = toask.issue;
        toask.answer = shuffle(toask.answer);
        let buttons = document.getElementsByClassName("qaswer");

        var el = document.getElementById("divquestion"),
                elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);

        for (let n = 0; n < buttons.length; n++) {
                buttons[n].innerHTML = toask.answer[n];
        }

        for (let n = 0; n < buttons.length; n++) {
                buttons[n].addEventListener("click", function(e) {
                        buttons[n].parentElement.style.background = "#fca613";
                       setTimeout(function(){
                         if (buttons[n].innerHTML === toask.ranswer) {
            score += scoremas[qnum - 1];
                         qnum++;
         document.getElementById("schet").innerHTML = score;

         setInterval(setanswer(), 500);
 } else {
         console.log(buttons[n].innerHTML + " | " + toask.ranswer);
         var el = document.getElementById("divquestion"),
                 elClone = el.cloneNode(true);
         el.parentNode.replaceChild(elClone, el);

         return false;
 } 

                        }, 500); 

                        /*if (buttons[n].innerHTML === toask.ranswer) {
                                score += scoremas[qnum - 1];
                                qnum++;
                                document.getElementById("schet").innerHTML = score;

                                setInterval(setanswer(), 500);
                        } else {
                                console.log(buttons[n].innerHTML + " | " + toask.ranswer);
                                var el = document.getElementById("divquestion"),
                                        elClone = el.cloneNode(true);
                                el.parentNode.replaceChild(elClone, el);

                                return false;
                        } */
                });
        }
}




function sleep(ms) {
ms += new Date().getTime();
while (new Date() < ms){}
} 

